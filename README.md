# vcard2gpx

Convert contact details to the
[GPS Exchange Format (GPX)](https://en.wikipedia.org/wiki/GPS_Exchange_Format)
for import in for example [OsmAnd](https://osmand.net/).

## Use

```shell
vcard2gpx *.vcf > contacts.gpx
```

## Test

```shell
pytest
```
