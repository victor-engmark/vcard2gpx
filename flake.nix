{
  description = "Application packaged using poetry2nix";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication;
      in
      {
        packages = {
          vcard2gpx = mkPoetryApplication { projectDir = self; };
          default = self.packages.${system}.vcard2gpx;
        };

        devShells.default = pkgs.mkShell {
          inputsFrom = [ self.packages.${system}.vcard2gpx ];
          packages = [ pkgs.poetry ];
        };
      });
}
